[[_TOC_]]

# Summary

This document was created from an initial email discussing [Repair Concerns](#repair-concerns).

It led to the creation of the World of Repair group, and the Project Management project here in Gitlab.

# Email Intro

    Thanks for sending this!

    This job has little to do with programming or arranging new software. Most of it is taking ideas and drilling them down into actionable steps, turning that into a business plan, and bringing them to life. For example, here's a few big picture items, where they are, and why a project manager would be very helpful.

# Response to Email Intro

Summarizing the initial [Repair Concerns](#repair-concerns) into section headers:

* Running A Repair Business
* Repair Guide for Everyone
* Why Right to Repair Is Important to You!
* Charity Repair/Repair Charity
* Finding Donors
* Implementing Right to Repair Laws

-------------

Extrapolating general categories based on that:

* Repair
  * Personal
  * Professional
  * Resources
  * Rights
    * Laws
  * Charity
    * Donations

----------

Merging [Repair Concerns](#repair-concerns) into general categories:

* Repair
  * Personal
  * Professional
    * Running A Repair Business
  * Resources
    * Repair Guide for Everyone
  * Rights
    * Why Right to Repair Is Important to You!
    * Laws
      * Implementing Right to Repair Laws
  * Charity
    * Charity Repair/Repair Charity
    * Donations
      * Finding Donors

# Repair Concerns

These started from the initial text of the email, and have given section headers in preparation for merging into general categories, and linking to other documentation within this project.

## Running A Repair Business

**Help repair shops administratively / help normal people learn how to run a repair business.** Most repair shops suck at running a business and make a ton of mistakes that lead them to failure or bad reviews. Some quit, some join CPR or ubreakifix, giving up a percentage of their income and becoming corporate tools in the process. I want to give them the ability to avoid this - so I created a subreddit where I've been aggregating standards people can vote on. https://www.reddit.com/r/repairstandards/ but, I haven't made the time to find top people who run independent repair businesses to help contribute theirs, and help us make a comprehensive, but general, manual on how to run a top tier repair business. I need to come up with a plan on how to do this, who will the target audience be, and what they need. Once I know that, how do I get there? What will it cost? This idea has fallen by the wayside for now - but none of these ideas should.

## Repair Guide for Everyone

**Help end users & repair shops alike with guides/plzbro information/research into how to repair new devices/how to solve confusing problems with new devices, including board repair** - I started repair.wiki and put all of my stuff up there. All of the info I know. But there's so much more, from much more talented technicians. I thought this would be as simple as, find the best people in the field, and offer them some money to contribute their solutions. It has not been that easy.

## Why Right to Repair Is Important to You!

**Get other people to understand right to repair being a problem/PR/advertising/shilling of this as an issue.** I got this with linus and MKBHD, but this is still a youtube audience. I need to break outside of this platform in driving awareness.,

## Charity Repair/Repair Charity

**Charity as in let's help people fix stuff(buy parts, have techs get paid to help assist underprivileged people fix their own stuff, etc)** We've been discussing doing a charitable event to get people into repair - but this would require extensive planning.

## Finding Donors

**Grant writing team to find NEW donors. there are way more people out there with millions burning a hole in their pocket they'd need to donate to a c3 or pay in taxes, and we need to find them!** We have some seed money for this 501c3, but it will eventually run out - and it's our job to get people on the hunt for the next donor, so that we can continue our mission. I don't pay myself a salary from this organization so this isn't going into my pocket, but rather to better the organization.

## Implementing Right to Repair Laws

**legally push right to repair forward(FTC complaints, etc)** - this I have been working on with a specific plan and staying on top of - admittedly, at the expense of the rest.

# Email Conclusion

    My head is everywhere. i need someone to take the ideas, write out plans on how to achieve them, with action items that have steps, then start walking us down the steps of how to achieve them - and to help me put a budget to each, and ensure that they do not fall by the wayside, but move forward.

    Is this something you think you're able & qualified to do? How does this relate to your past experience and what you do now?

    Thank you!

# Response to Email Conclusion

Yes, I am able and qualified to do this.

This relates to my past experience, and what I do now, because I enjoy the process of managing complexity, and have experience in doing so.

This document serves as an initial first example of the type of work I do.

You said "My head is everywhere."  I completely understand.  At this moment, there are thousands of thoughts all responding to your reply to my initial email, and to the questons of your conclusion, and each touches upon a vast array of history/memory that I have consistently cleaned up and organized.

"The document is nothing; the documenting is everything."

Thank you for sharing a small subset of your mental model regarding repair with me, and taking the time to encode that in a document, and gift it to me.

This document is my corresponding gift to you.

And thus we begin the process of documenting, a vital creative step and an anchor for subsequent action.

