
Thanks for sending this!

This job has little to do with programming or arranging new software. Most of it is taking ideas and drilling them down into actionable steps, turning that into a business plan, and bringing them to life. For example, here's a few big picture items, where they are, and why a project manager would be very helpful.



**Help repair shops administratively / help normal people learn how to run a repair business.** Most repair shops suck at running a business and make a ton of mistakes that lead them to failure or bad reviews. Some quit, some join CPR or ubreakifix, giving up a percentage of their income and becoming corporate tools in the process. I want to give them the ability to avoid this - so I created a subreddit where I've been aggregating standards people can vote on. https://www.reddit.com/r/repairstandards/ but, I haven't made the time to find top people who run independent repair businesses to help contribute theirs, and help us make a comprehensive, but general, manual on how to run a top tier repair business. I need to come up with a plan on how to do this, who will the target audience be, and what they need. Once I know that, how do I get there? What will it cost? This idea has fallen by the wayside for now - but none of these ideas should.

**Help end users & repair shops alike with guides/plzbro information/research into how to repair new devices/how to solve confusing problems with new devices, including board repair** - I started repair.wiki and put all of my stuff up there. All of the info I know. But there's so much more, from much more talented technicians. I thought this would be as simple as, find the best people in the field, and offer them some money to contribute their solutions. It has not been that easy.

**Get other people to understand right to repair being a problem/PR/advertising/shilling of this as an issue.** I got this with linus and MKBHD, but this is still a youtube audience. I need to break outside of this platform in driving awareness.,

**Charity as in let's help people fix stuff(buy parts, have techs get paid to help assist underprivileged people fix their own stuff, etc)** We've been discussing doing a charitable event to get people into repair - but this would require extensive planning.

**Grant writing team to find NEW donors. there are way more people out there with millions burning a hole in their pocket they'd need to donate to a c3 or pay in taxes, and we need to find them!** We have some seed money for this 501c3, but it will eventually run out - and it's our job to get people on the hunt for the next donor, so that we can continue our mission. I don't pay myself a salary from this organization so this isn't going into my pocket, but rather to better the organization.

**legally push right to repair forward(FTC complaints, etc)** - this I have been working on with a specific plan and staying on top of - admittedly, at the expense of the rest.

My head is everywhere. i need someone to take the ideas, write out plans on how to achieve them, with action items that have steps, then start walking us down the steps of how to achieve them - and to help me put a budget to each, and ensure that they do not fall by the wayside, but move forward.

Is this something you think you're able & qualified to do? How does this relate to your past experience and what you do now?

Thank you!
